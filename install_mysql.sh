#!/bin/bash

curdir=$(cd `dirname $0`; pwd) 

# 是否开始
function isStart(){
    read -p "是否开始一键部署MySQL？（Y/N）:" choose_isStart
    if [ "$choose_isStart" == 'Y' ] || [ "$choose_isStart" == 'y' ] ;then
        return 0  # 退出函数
    else
        exit 0  # 退出程序
    fi
}

# 检查用户
function use_check(){
    curuser=$USER
    if [ $curuser != 'root' ];then
        echo "当前用户非root,请切换至root后再操作！" 
        exit 0
    else
        return 0
    fi
}

# ******************************** rpmType ********************************

# 检查mariadb
function mariadb_check(){
    mariadb_rpm_list=`rpm -qa |grep mariadb`
    if [ -n "$mariadb_rpm_list" ];then
        echo "mariadb检查结果：系统已安装mariadb,需先卸载"
        read -p "是否卸载mariadb？（Y/N）:" choose_Unmariadb
        if [ "$choose_Unmariadb" == 'Y' ] || [ "$choose_Unmariadb" == 'y' ] ;then
            echo "开始卸载mariadb..."
            rpm -qa |grep mariadb |xargs rpm -e --nodeps
            sleep 2
            echo "mariadb卸载完成！"
        else
            exit 0
        fi
    else
        echo "mariadb检查结果：系统未安装mariadb."
        return 0
    fi
}


# 检查MySQL
function mysql_check(){
    mysql_rpm_list=`rpm -qa |grep mysql`
    if [ -n "$mysql_rpm_list" ]; then
        echo "mysql检查结果：系统已安装mysql,需先卸载,已安装的MySQL如下："
        rpm -qa | grep mysql | tee
        read -p "是否卸载已安装的MySQL？(Y/N)：" choose_UnMySQL
        if [ "$choose_UnMySQL" == 'Y' ] || [ "$choose_UnMySQL" == 'y' ];then
            echo "开始卸载MySQL..."
            rpm -qa |grep mysql | xargs rpm -e --nodeps  > /dev/null 2>&1
            sleep 2
            echo "MySQL卸载完成！"
        else
            exit 0
        fi
    else
        echo "mysql检查结果：系统未安装MySQL."
    fi
}


# 检查可能的MySQL残留文件
function mysqlFile_check(){
    mysqlFile=`find / -name mysql`
    if [ -n "$mysqlFile" ]; then
        echo "检测到服务器可能存在MySQL残留文件，文件列表如下："
        find / -name mysql |xargs du --max-depth=0 -h   # 打印可能的MySQL残留文件及大小
        read -p "您要对这些文件怎么处理？（0:正常文件，不处理；1:我要手动删除；del:全部删除）：" choose_delmysqlFile
        if [ "$choose_delmysqlFile" == 0 ] ;then
            return 0
        elif [ "$choose_delmysqlFile" == 1 ] ;then
            exit 0
        elif [ "$choose_delmysqlFile" == 'del' ] ;then
            echo "开始清理MySQL残留文件..."
            #清理冗余文件
            find / -name mysql |xargs rm -rf
            sleep 3
            rm -f /var/log/mysqld.log
            echo "MySQL残留文件清理成功！"
        else
            echo "输入错误，请选择0、1、del."
            exit 0
        fi
    else
        rm -f /var/log/mysqld.log
        echo "mysql残留文件检查结果：不存在MySQL残留文件"
    fi
}


# 安装MySQL
function mysql_install_rpmType(){
    echo "关闭SELinux..."  
    setenforce 0   > /dev/null 
    echo "关闭防火墙..."
    systemctl stop firewalld.service
	​echo "下载MySQL"
	wget http://repo.mysql.com/mysql-community-release-el7-5.noarch.rpm
    echo "开始安装MySQL源"
    rpm -ivh mysql-community-release-el7-5.noarch.rpm
	echo "开始安装MySQL..."
	yum install mysql-server -y
    sleep 5
    echo "MySQL安装成功，开始启动MySQL..."
    systemctl start mysqld.service
    if [ $? -eq 0 ] ;then
        echo "MySQL启动成功！"
    else
        echo "MySQL启动失败，请检查日志/var/log/mysqld.log"
        exit 0
    fi
}


# 修改MySQL密码
function change_passwd_rpmType(){
    initpasswd=`cat /var/log/mysqld.log |grep root@localhost |awk '{print $NF}'`   # 初始密码
    echo "MySQL的初始密码是：$initpasswd"
    read -p "请输入您要设置MySQL的root用户密码：" passwd
    export MYSQL_PWD=$initpasswd  #解决mysql>5.6会提示密码安全问题
    mysqladmin -uroot password $passwd   > /dev/null
    echo $?
    echo "MySQL密码修改成功！"
}


# 修改MySQL权限
function change_access_rpmType(){
    export MYSQL_PWD=$passwd
    mysql -uroot <<EOF
    use mysql;
    update user set Host="%" where User="root";
    flush privileges;
	quit
EOF
	systemctl restart mysqld
    echo "MySQL权限修改成功！"
}
  




# 迁移目录
function Migration_dir(){
    read -p "是否需要迁移MySQL数据目录？(Y/N)：" choose_isMigration
    if [ "$choose_isMigration" == 'Y' ] || [ "$choose_isMigration" == 'y' ] ;then 
        read -p "请输入您要迁移至的路径（如：/opt/data）：" newPath
        echo "开始准备迁移目录..."
        systemctl stop mysqld.service
        mkdir -p $newPath   # 新建目录
        cp -r /var/lib/mysql $newPath
        chmod -R 755 $newPath
        chown -R mysql:mysql $newPath
        # 修改my.cnf
        # 将迁移的路径进行转义处理，方便使用sed进行替换修改
        newPathe=`echo $newPath |sed 's#\/#\\\/#g'`
        sed -i "s/\/var\/lib/$newPathe/g" /etc/my.cnf
        # 插入配置
        sed -i "1i\socket=$newPath/mysql/mysql.sock" /etc/my.cnf
        sed -i "1i\[client]" /etc/my.cnf
        systemctl start mysqld.service
        echo "MySQL目录迁移成功！"
    else
        return 0
    fi
}


# 检查安装结果
function mysql_successful_rpmType(){
    netstat -ntl | grep 3306
    if [ $? -eq 0 ] ;then
        echo "MySQL安装成功，现在可以开始正式使用啦！"
    else
        echo "MySQL安装或启动失败，请查看日志:/var/log/mysqld.log"
    fi
}


# ******************************** 函数调用 ********************************

# rpm类型安装
function rpmType(){
    isStart  # 是否开始
    use_check  # 检查用户
    mariadb_check  # 检查mariadb  
    mysql_check  # 检查MySQL
    mysqlFile_check  # 检查残留文件
    #mysql_PackageName_chech_rpmType  # 检查安装包名称 + 安装MySQL
	mysql_install_rpmType  # 安装MySQL
	change_passwd_rpmType  # 修改密码
    change_access_rpmType  # 修改权限
    Migration_dir  # 迁移目录
    mysql_successful_rpmType  # 安装结果检查
	
}
rpmType


