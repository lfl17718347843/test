#!/bin/bash

# 服务名
name="nacos-server"

# 过滤nacos-server进程
ID=`ps -ef | grep "$name" | grep -v "grep" | awk 'NR==1{print $2}'`

# 杀掉进程
kill -9 $ID

# 路径
nacos_server_path=/data/nacos/

# 启动服务
cd $nacos_server_path && \
sh bin/startup.sh -m standalone