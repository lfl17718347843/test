#!/bin/bash

# 安装依赖环境jdk-11
yum install -y java-11* 2>&1 >/dev/null

# 下载nacos包
wget https://github.com/alibaba/nacos/releases/download/1.4.2/nacos-server-1.4.2.tar.gz 

# 解压nacos包并启动脚本
tar xf nacos-server-1.4.2.tar.gz && \
cd nacos && sh bin/startup.sh -m standalone && \

# 删除tar包
rm -rf /root/nacos-server-1.4.2.tar.gz