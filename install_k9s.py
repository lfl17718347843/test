#!/usr/bin/env python
# -*- coding: UTF-8 -*-

# 导入subprosess模块
import os 
import subprocess

# 创建install函数
def install():
    subprocess.call(['wget','https://github.com/derailed/k9s/releases/download/v0.27.4/k9s_Linux_amd64.tar.gz'])
    subprocess.call(['tar','zxf','k9s_Linux_amd64.tar.gz'])
    subprocess.call(['mv','k9s','/usr/local/bin/'])
    subprocess.call(['rm','-rf','k9s_Linux_amd64.tar.gz'])
    subprocess.call(['k9s','version'])
    print("k9s安装完成")

# 函数主程序入口
if __name__ == "__main__":
    install()
