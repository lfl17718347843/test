#!/bin/bash

# 定义函数
install_nginx() {
   # 创建目录
   sudo mkdir /home/ec2-user && \
   sudo mkdir -p /data/shell && \
   sudo mkdir -p /alidata/website/bak && \
   sudo mkdir /usr/local/nginx
   # 追加以下内容到limits.conf
   echo '* soft nofile 1000000' >> /etc/security/limits.conf
   echo '* hard nofile 1000000' >> /etc/security/limits.conf
   # 追加以下内容到sysctl.conf
   echo 'fs.file-max = 1000000' >> /etc/sysctl.conf
   sysctl -p 
   # 同步时间
   timedatectl set-timezone Asia/Saigon 
   yum -y install chrony 
   echo server 169.254.169.123 iburst >> /etc/chrony.conf && \
   systemctl enable chronyd && \
   systemctl start chronyd && \
   chronyc sourcestats
   # 安装nginx依赖环境
   yum -y install zip unzip wget lrzsz gcc gcc-c++ pcre pcre-devel zlib zlib-devel openssl openssl-devel libssl-dev libpcre3 libpcre3-dev make 2>&1 >/dev/null 
   # 下载并解压
   wget http://nginx.org/download/nginx-1.18.0.tar.gz 2>&1 >/dev/null
   tar -zxf nginx-1.18.0.tar.gz
   # 进入文件夹并配置安装路径和编译模块
   cd nginx-1.18.0 && ./configure --prefix=/usr/local/nginx --with-http_stub_status_module --with-http_ssl_module --with-file-aio --with-http_realip_module 
   # 编译安装
   make -j8 && make install 
   # 启动nginx
   /usr/local/nginx/sbin/nginx 
   # 查看nginx是否启动成功
   ps -ef |grep nginx 
   # 删除tar包
   rm -rf /root/nginx-1.18.0.tar.gz 
}
install_nginx 
