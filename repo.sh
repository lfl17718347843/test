#!/bin/bash
##############################
#   解决默认镜像源失败问题   #
##############################

# 备份默认镜像源
mv /etc/yum.repos.d/CentOS-Base.repo /etc/yum.repos.d/CentOS-Base.repo.bak

# 下载最新镜像源
curl -o /etc/yum.repos.d/CentOS-Base.repo https://mirrors.aliyun.com/repo/Centos-7.repo

# 清除 yum 缓存并重新生成 yum 缓存 
yum clean all && yum makecache
